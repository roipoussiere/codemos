# Contributing to Codemos

Thank you! You have several ways to contribute to Codemos.

## 🗩 Create issues

You can [create issues](https://framagit.org/roipoussiere/codemos/-/issues) to submit bug reports or features proposals.

## ✏️ Writing code

You must install and setup Codemos from sources.

### Installation

    git clone https://framagit.org/roipoussiere/codemos.git
    cd codemos
    make build

### Configuration

    cp app.env.dist app.env
    nano app.env

You also might be interested to add this line to your `/etc/hosts`:

    127.0.0.1  codemos.qrok.me

This will trick the Github authentication system that check if the `GH_REDIRECT_URI` parameter matches the uri defined in the app config. This way you can work on localhost without updating the Github app config (note that the app port must be 80).

### Start

    ./bin/codemos

or `sudo ./bin/codemos` if you work on port 80.

You could eventually pass environment variables without editing config file:

    APP_PORT=8080 ./bin/codemos

### Check and format code

    make check
    make format

Check other make options with `make help`.