package main

import (
	"log"

	"github.com/spf13/viper"
)

// Config structure that handle configuration.
type Config struct {
	AppPort        int    `mapstructure:"APP_PORT"`
	GhClientID     string `mapstructure:"GH_CLIENT_ID"`
	GhClientSecret string `mapstructure:"GH_CLIENT_SECRET"`
}

func getConfig() Config {
	var config Config

	conf := viper.New()
	conf.SetConfigName("app")
	conf.SetConfigType("env")
	conf.AddConfigPath("$HOME/.config/codemos")
	conf.AddConfigPath(".")

	conf.SetDefault("APP_PORT", 80)
	conf.SetDefault("GH_CLIENT_ID", "")
	conf.SetDefault("GH_CLIENT_SECRET", "")

	conf.AutomaticEnv()

	err := conf.ReadInConfig()
	if err != nil {
		log.Println("Config file not found, using env variable...")
	}

	err = conf.Unmarshal(&config)
	if err != nil {
		log.Fatalf("Error when parsing config file: %w", err)
	}

	log.Println("Configuration:")
	for k, v := range conf.AllSettings() {
		log.Printf("- %s: %v", k, v)
	}

	return config
}
