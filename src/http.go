package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func httpGet(url string, token string, target interface{}) {
	request, err1 := http.NewRequest("GET", url, nil)
	if err1 != nil {
		log.Fatalf("Error when creating http request: %v", err1)
	}

	request.Header.Add("Accept", "application/json")
	request.Header.Add("Authorization", "token "+token)

	client := &http.Client{}

	response, err2 := client.Do(request)
	if err2 != nil {
		log.Fatalf("Error when executing http request: %v", err2)
	}
	defer response.Body.Close()

	err3 := json.NewDecoder(response.Body).Decode(target)
	if err3 != nil {
		log.Fatalf("Error when parsing http request result: %v", err3)
	}
}
