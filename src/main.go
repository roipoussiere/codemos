package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

var (
	config Config = getConfig()
)

func main() {
	router := gin.Default()
	router.Static("/app", "./static")

	router.GET("/", root)
	router.GET("/gh_auth", ghAuth)
	router.GET("/gh_user_info", ghUserInfo)

	router.Run(fmt.Sprintf(":%v", config.AppPort))
}

func root(c *gin.Context) {
	c.Redirect(http.StatusFound, "/app/index.html")
}
