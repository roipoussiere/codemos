package main

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

var (
	baseAPIEndpoint = "https://api.github.com"
)

func ghAuth(context *gin.Context) {
	const ghAccessTokenURL = "https://github.com/login/oauth/access_token"

	postBody, _ := json.Marshal(map[string]string{
		"client_id":     config.GhClientID,
		"client_secret": config.GhClientSecret,
		"code":          context.Query("code"),
	})

	request, err := http.NewRequest("POST", ghAccessTokenURL, bytes.NewBuffer(postBody))
	if err != nil {
		log.Fatalf("error when creating http request: %v", err)
	}

	request.Header.Add("Accept", "application/json")
	request.Header.Add("Content-Type", "application/json")

	client := &http.Client{}

	response, err := client.Do(request)
	if err != nil {
		log.Fatalf("error when executing http request: %v", err)
	}
	defer response.Body.Close()

	var result map[string]interface{}
	json.NewDecoder(response.Body).Decode(&result)

	if errorMsg, ok := result["error"].(string); ok {
		if errorDetails, ok := result["error_description"].(string); ok {
			errorMsg += "\n" + errorDetails
		}
		log.Fatalf("received an error from http server: %s", errorMsg)
	}

	if token, ok := result["access_token"].(string); ok {
		log.Printf("retrieved access token: %s", token)
		context.JSON(200, gin.H{
			"access_token": token,
		})
	}
}

func ghUserInfo(context *gin.Context) {
	var token = context.Query("token")

	user := new(User)
	httpGet(baseAPIEndpoint+"/user", token, user)

	repos := new(Repos)
	httpGet(baseAPIEndpoint+"/user/repos", token, repos)

	context.JSON(200, gin.H{
		"user":  user,
		"repos": repos,
	})
}
