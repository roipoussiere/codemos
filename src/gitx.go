package main

// User structure that represent a user in a Gitx platform
type User struct {
	ID    int    `json:"id"`
	Login string `json:"login"`
	Name  string `json:"name"`
	URL   string `json:"url"`
	Email string `json:"email"`
}

// Repo structure that represent a repository in a Gitx platform
type Repo struct {
	ID          int    `json:"id"`
	FullName    string `json:"full_name"`
	Name        string `json:"name"`
	URL         string `json:"url"`
	Description string `json:"description"`
}

// Repos structure that represent a list of repositories (type Repo)
type Repos []Repo
