document.addEventListener('alpine:init', () => {
	Alpine.store('links', {
		gh_auth_url: get_gh_auth_url()
	})

	Alpine.store('gh_user_info', { connected: false })
})

document.addEventListener('DOMContentLoaded', async () => {
	await update_gh_user_info()
})

function get_gh_code() {
	const url_params = new URLSearchParams(window.location.search)
	const params = Object.fromEntries(url_params.entries())

	if (params.error) {
		console.error(params.error)
		console.error(params.error_description)
	} else if (params.code) {
		return params.code
	}
}

async function get_gh_token() {
	if (sessionStorage.gh_token) {
		return sessionStorage.gh_token
	}

	const gh_code = get_gh_code()
	if (! gh_code) {
		return
	}

	params = { code: gh_code }

	try {
		reponse = await axios.get('/gh_auth', { params: params })
		sessionStorage.gh_token = reponse.data.access_token
	} catch (error) {
		console.error(error)
	}
}

async function update_gh_user_info() {
	const gh_token = await get_gh_token()
	if (! gh_token) {
		return
	}

	params = { token: sessionStorage.gh_token }

	try {
		response = await axios.get('/gh_user_info', { params: params })
		gh_user_info = Object.assign({}, response.data, {'connected': true})
		Alpine.store('gh_user_info', gh_user_info)
	} catch (error) {
		console.error(error)
	}
}

function get_gh_auth_url() {
	const GH_CLIENT_ID = '562f2a8dbd7cf5eedb57'
	// const GH_REDIRECT_URI = 'http://codemos.qrok.me/app/index.html'
	const GH_AUTH_URL_ENDPOINT = 'https://github.com/login/oauth/authorize'

	var query = new URLSearchParams()
	query.append('client_id', GH_CLIENT_ID)
	// query.append('redirect_uri', GH_REDIRECT_URI)
	query.append('scope', 'read:user')
	// query.append('state', 'unguessable random string')
	return GH_AUTH_URL_ENDPOINT + '?' + query.toString()
}
