# Codemos

Bits of democracy in your code workflow.

Want to try now? Check the [demo website](https://codemos.qrok.me).

*This project is under development and is not usable yet, please come back later. :)*

## 🌿 What is Codemos?

Codemos is a voting platform designed to work with an issue tracking system (ie. Github or Gitlab), with [majority judgment](https://en.wikipedia.org/wiki/Majority_judgment).

Software developers creates polls on a group of issues, helping them to understand users needs.

## 🏗️ Installation

### Using Docker

    git clone https://framagit.org/roipoussiere/codemos.git
    cd codemos
    sudo docker build --tag codemos .

## ⚙️ Configuration

You must configure Codemos according to your needs.

See the [default config](./app.env.dist) for more information about available configuration variables.

### Using Docker

For production it's recommended to configure Codemos from environment variables, that can be stored in `app.env` file:

    cp app.env.dist app.env
    nano env

## 🚀 Start

### Using Docker

Start your container using the Codemos image you just created, and define app port and env file path:

    sudo docker run --rm --name my-codemos -p 80:80 --env-file app.env codemos

## ❤️ Contributing

See the [contribution guide](CONTRIBUTING.md).

## 📜 License & ownership

- license: [MIT](LICENSE)
- author: Nathanaël Jourdane, for the [Mieux Voter](https://mieuxvoter.fr/) non-profit organization.
- contact: roipoussiere · protonmail · com
