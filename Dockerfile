FROM golang:1.16-alpine

WORKDIR /codemos

COPY . .

RUN go build -o ./bin/codemos ./src

EXPOSE 80

CMD ["./bin/codemos"]
